from django.urls import path

from .views import *

app_name = 'polls'
urlpatterns = [
    # ex: /polls/
    path('', IndexView.as_view(), name='index'),
    # ex: /polls/5/
    path('<int:pk>/', DetailView.as_view(), name='detail'),
    # ex: /polls/5/results/
    path('<int:pk>/results/', ResultsView.as_view(), name='results'),
    # ex: /polls/5/vote/
    path('<int:pk>/vote/', vote, name='vote'),
]